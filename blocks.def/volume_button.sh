#!/bin/sh
case "$1" in
    1) pactl set-sink-mute @DEFAULT_SINK@ toggle ;;
    3) alacritty -e /bin/bash -c 'ncpamixer';;
esac
